//
//  HomeRouter.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 21..
//

final class HomeRouter: Router<HomeViewController>, HomeRouter.Routes {
    typealias Routes = AlertRoute
}
