//
//  HomeViewModel.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 21..
//

import RxSwift
import RxCocoa

final class HomeViewModel: BViewModel {
    // MARK: - Types

    struct Input {
        let fetch: Driver<Void>
        let retry: Driver<Void>
    }

    struct Output {
        let characterViewModels: Driver<[HomeCharacterViewModel]>
        let error: Driver<Int>
    }

    // MARK: - Properties

    let router: HomeRouter.Routes
    private let seriesRepository: SeriesRepositoryProtocol
    private var maxPage: Int = 1

    // MARK: - Initialization

    init(router: HomeRouter.Routes,
         seriesRepository: SeriesRepositoryProtocol) {
        self.router = router
        self.seriesRepository = seriesRepository
    }

    // MARK: - Functions

    func transform(input: Input) -> Output {
        let errorTracker = ErrorTracker()

        let fetch = input.fetch
            .scan(0) { lastCount, _ in
                return lastCount + 1
            }
            .filter { [weak self] pages in
                return pages <= self?.maxPage ?? 0
            }

        let retry = input.retry.withLatestFrom(fetch)

        let trigger = Driver.merge(fetch, retry)

        let charactersResponse = fetchCharacters(trigger: trigger, errorTracker: errorTracker)
            .map { [weak self] response -> CharacterResponse in
                self?.maxPage = response.info.pages

                return response
            }

        let createViewModels = createViewModels(from: charactersResponse)
            .scan([], accumulator: { lastViewModuls, newViewModules in
                return lastViewModuls + newViewModules
            })
            .asDriver()

        let errors = errorTracker
            .flatMap { [weak self] error -> Driver<Int> in
                guard let self = self else { return Driver.empty() }

                return self.router.openErrorAlert(error: error)
                    .asDriverOnErrorJustComplete()
            }.asDriver()

        return Output(characterViewModels: createViewModels,
                      error: errors)
    }

    private func fetchCharacters(trigger: Driver<Int>, errorTracker: ErrorTracker) -> Driver<CharacterResponse> {
        return trigger
            .flatMapLatest { [weak self] page -> Driver<CharacterResponse> in
                guard let self = self else { return .empty() }

                return self.seriesRepository.character(page: page)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }
    }

    private func createViewModels(from response: Driver<CharacterResponse>) -> Driver<[HomeCharacterViewModel]> {
        let createdViewModels = response.map { response -> [HomeCharacterViewModel] in
            return response.results.map { character in
                let viewModel = HomeCharacterViewModel(id: character.id,
                                                       imageURL: character.image,
                                                       name: character.name,
                                                       status: character.status.rawValue.capitalizingFirstLetter(),
                                                       origin: character.origin.name)

                return viewModel
            }
        }

        return createdViewModels
    }
}
