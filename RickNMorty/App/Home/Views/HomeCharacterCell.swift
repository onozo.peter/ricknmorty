//
//  HomeCharacterCell.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 24..
//

import UIKit
import AlamofireImage

class HomeCharacterCell: BTableViewCell {
    // MARK: - Properties

    lazy var characterImageView: UIImageView = {
        let characterImageView = UIImageView()
        characterImageView.translatesAutoresizingMaskIntoConstraints = false
        characterImageView.contentMode = .scaleToFill

        return characterImageView
    }()

    lazy var nameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.font = UIFont.preferredFont(forTextStyle: .body)
        nameLabel.numberOfLines = 1
        nameLabel.textColor = .black

        return nameLabel
    }()

    lazy var originLabel: UILabel = {
        let originLabel = UILabel()
        originLabel.translatesAutoresizingMaskIntoConstraints = false
        originLabel.font = UIFont.preferredFont(forTextStyle: .body)
        originLabel.numberOfLines = 2
        originLabel.textColor = .black

        return originLabel
    }()

    lazy var leftContainer: BView = {
        let leftContainer = BView()
        leftContainer.translatesAutoresizingMaskIntoConstraints = false

        return leftContainer
    }()

    lazy var statusLabel: UILabel = {
        let statusLabel = UILabel()
        statusLabel.translatesAutoresizingMaskIntoConstraints = false
        statusLabel.font = UIFont.preferredFont(forTextStyle: .body)
        statusLabel.numberOfLines = 1
        statusLabel.textColor = .black

        return statusLabel
    }()

    // MARK: - BTableViewCell Functions

    override func createView() {
        super.createView()

        contentView.addSubview(characterImageView)
        contentView.addSubview(leftContainer)
        leftContainer.addSubview(nameLabel)
        leftContainer.addSubview(originLabel)
        contentView.addSubview(statusLabel)
    }

    override func initializeView() {
        super.initializeView()

        selectionStyle = .none
    }

    override func setupConstraints() {
        super.setupConstraints()

        let characterImageConstraints = [
            characterImageView.widthAnchor.constraint(equalToConstant: 60),
            characterImageView.heightAnchor.constraint(equalToConstant: 60),
            characterImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            characterImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16)
        ]
        NSLayoutConstraint.activate(characterImageConstraints)

        let leftContainerConstraints = [
            leftContainer.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            leftContainer.leftAnchor.constraint(equalTo: characterImageView.rightAnchor, constant: 16),
            contentView.bottomAnchor.constraint(equalTo: leftContainer.bottomAnchor, constant: 16)
        ]
        NSLayoutConstraint.activate(leftContainerConstraints)

        let nameLabelConstraints = [
            nameLabel.topAnchor.constraint(equalTo: leftContainer.topAnchor),
            nameLabel.leftAnchor.constraint(equalTo: leftContainer.leftAnchor),
            leftContainer.rightAnchor.constraint(equalTo: nameLabel.rightAnchor)
        ]
        NSLayoutConstraint.activate(nameLabelConstraints)

        let originLabelConstraints = [
            originLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8),
            originLabel.leftAnchor.constraint(equalTo: leftContainer.leftAnchor),
            leftContainer.bottomAnchor.constraint(equalTo: originLabel.bottomAnchor),
            leftContainer.rightAnchor.constraint(equalTo: originLabel.rightAnchor)
        ]
        NSLayoutConstraint.activate(originLabelConstraints)

        let statusLabelConstraints = [
            statusLabel.centerYAnchor.constraint(equalTo: leftContainer.centerYAnchor),
            statusLabel.leftAnchor.constraint(equalTo: leftContainer.rightAnchor, constant: 16),
            contentView.rightAnchor.constraint(equalTo: statusLabel.rightAnchor, constant: 16)
        ]
        NSLayoutConstraint.activate(statusLabelConstraints)

        statusLabel.setContentHuggingPriority(UILayoutPriority.required, for: .horizontal)
        statusLabel.setContentCompressionResistancePriority(UILayoutPriority.required, for: .horizontal)
    }

    // MARK: - Functions

    func setupImage(_ imageUrl: String) {
        guard let url = URL(string: imageUrl) else { return }

        characterImageView.af.setImage(withURL: url)
    }
}
