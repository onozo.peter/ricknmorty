//
//  HomeViewController.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 21..
//

import RxSwift
import RxCocoa
import RxDataSources

final class HomeViewController: BViewController {
    // MARK: - Types

    typealias HomeSectionModel = AnimatableSectionModel<String, HomeCharacterViewModel>

    // MARK: - Properties

    let viewModel: HomeViewModel
    let retry = PublishSubject<Void>()

    var tableDataSource: RxTableViewSectionedAnimatedDataSource<HomeSectionModel>?

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .white
        tableView.estimatedRowHeight = 92
        tableView.rowHeight = UITableView.automaticDimension

        return tableView
    }()

    private var configureCell: RxTableViewSectionedAnimatedDataSource<HomeSectionModel>.ConfigureCell {
        return { _, tableView, indexPath, viewModel in
            let cell: HomeCharacterCell = tableView.dequeueReusableCell(for: indexPath)

            cell.setupImage(viewModel.imageURL)
            cell.nameLabel.text = viewModel.name
            cell.statusLabel.text = viewModel.status
            cell.originLabel.text = viewModel.origin
            cell.accessoryType = .disclosureIndicator

            return cell
        }
    }

    // MARK: - Initialization

    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel

        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - SCViewController functions

    override func createView() -> BView {
        let view = super.createView()

        view.addSubview(tableView)

        return view
    }

    override func initializeView() {
        super.initializeView()

        title = "home__title".localized

        view.backgroundColor = .white
        navigationController?.navigationBar.prefersLargeTitles = true

        tableView.register(HomeCharacterCell.self, forCellReuseIdentifier: HomeCharacterCell.reuseID)
    }

    override func bindViewModel() {
        super.bindViewModel()

        tableDataSource = RxTableViewSectionedAnimatedDataSource<HomeSectionModel>(
            animationConfiguration: AnimationConfiguration(insertAnimation: .left,
                                                           reloadAnimation: .none,
                                                           deleteAnimation: .none),
            configureCell: configureCell)

        transformViewModelInput(createViewModelInput())
    }

    override func setupViewConstraints() {
        super.setupViewConstraints()

        let tableConstraints = [
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            view.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
            view.rightAnchor.constraint(equalTo: tableView.rightAnchor)
        ]
        NSLayoutConstraint.activate(tableConstraints)
    }

    // MARK: - Functions

    private func createViewModelInput() -> HomeViewModel.Input {
        let fetch = Driver.just(())

        let retry = retry.asDriverOnErrorJustComplete()

        return HomeViewModel.Input(fetch: fetch, retry: retry)
    }

    private func transformViewModelInput(_ input: HomeViewModel.Input) {
        let output = viewModel.transform(input: input)

        output.characterViewModels
            .map { viewModels in
                return [HomeSectionModel(model: "", items: viewModels )]
            }
            .drive(tableView.rx.items(dataSource: tableDataSource!))
            .disposed(by: disposeBag)

        output.error
            .drive(onNext: { [weak self] buttonIndex in
                guard let self = self else { return }

                if buttonIndex == 0 {
                    self.retry.on(.next(()))
                }
            })
            .disposed(by: disposeBag)
    }
}
