//
//  HomeModule.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 21..
//

import Foundation

final class HomeModule {
    // MARK: - Properties

    let router: HomeRouter
    let viewController: HomeViewController

    private let viewModel: HomeViewModel

    // MARK: - Initialization

    init() {
        let seriesRepository = coreAssembler.resolveSeriesRepository()

        let router = HomeRouter()
        let viewModel = HomeViewModel(router: router,
                                      seriesRepository: seriesRepository)
        let viewController = HomeViewController(viewModel: viewModel)

        router.viewController = viewController

        self.router = router
        self.viewController = viewController
        self.viewModel = viewModel
    }
}
