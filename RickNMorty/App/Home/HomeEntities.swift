//
//  HomeEntities.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 21..
//

import RxDataSources

struct AnimatableSectionModel<Section: IdentifiableType, ItemType: IdentifiableType & Equatable> {
    var model: Section
    var items: [Item]

    init(model: Section, items: [ItemType]) {
        self.model = model
        self.items = items
    }
}

extension AnimatableSectionModel: AnimatableSectionModelType {
    typealias Item = ItemType
    typealias Identity = Section.Identity

    var identity: Section.Identity {
        return model.identity
    }

    init(original: AnimatableSectionModel, items: [Item]) {
        self.model = original.model
        self.items = items
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(model.identity)
    }
}

extension AnimatableSectionModel: Equatable where Section: Equatable {
    static func == (lhs: AnimatableSectionModel, rhs: AnimatableSectionModel) -> Bool {
        return lhs.model == rhs.model
            && lhs.items == rhs.items
    }
}

struct HomeCharacterViewModel {
    let id: Int
    let imageURL: String
    let name: String
    let status: String
    let origin: String
}

extension HomeCharacterViewModel: Hashable {
    static func == (lhs: HomeCharacterViewModel, rhs: HomeCharacterViewModel) -> Bool {
        return lhs.id == rhs.id
    }
}

extension HomeCharacterViewModel: IdentifiableType {
    var identity: String {
        return String(id)
    }

    typealias Identity = String
}
