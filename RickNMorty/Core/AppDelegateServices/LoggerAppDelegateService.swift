//
//  LoggerAppDelegateService.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import UIKit
import XCGLogger

public final class LoggerAppDelegateService: NSObject {
    // MARK: - Properties

    let isDebug: Bool

    private let cachesDirectoryPath: URL = {
        guard let cachesDirectoryPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).last else {
            warningLog("Couldn't find the caches directory path.")
            return URL(fileURLWithPath: "")
        }

        return cachesDirectoryPath
    }()

    // MARK: - Initialization

    public init(isDebug: Bool) {
        self.isDebug = isDebug
    }
}

// MARK: - UIApplicationDelegate

extension LoggerAppDelegateService: UIApplicationDelegate {
    private struct PrivateConstants {
        static let outputFileName = "XCGLogger_Log.txt"
    }

    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        let xcgLogger = XCGLogger.default

        guard isDebug else {
            xcgLogger.outputLevel = .none
            return true
        }

        let outputFilePath = cachesDirectoryPath.appendingPathComponent(PrivateConstants.outputFileName)
        xcgLogger.setup(level: .debug, showThreadName: true, writeToFile: outputFilePath)

        if let fileDestination = xcgLogger.destination(withIdentifier: XCGLogger.Constants.fileDestinationIdentifier) as? FileDestination {
            let ansiColorLogFormatter: ANSIColorLogFormatter = ANSIColorLogFormatter()
            ansiColorLogFormatter.colorize(level: .verbose, with: .colorIndex(number: 244), options: [.faint])
            ansiColorLogFormatter.colorize(level: .debug, with: .black)
            ansiColorLogFormatter.colorize(level: .info, with: .blue, options: [.underline])
            ansiColorLogFormatter.colorize(level: .warning, with: .red, options: [.faint])
            ansiColorLogFormatter.colorize(level: .error, with: .red, options: [.bold])
            ansiColorLogFormatter.colorize(level: .severe, with: .white, on: .red)
            fileDestination.formatters = [ansiColorLogFormatter]
        }

        let emojiLogFormatter = PrePostFixLogFormatter()
        emojiLogFormatter.apply(prefix: "🗯 ", postfix: " 🗯", to: .verbose)
        emojiLogFormatter.apply(prefix: "🔹 ", postfix: " 🔹", to: .debug)
        emojiLogFormatter.apply(prefix: "ℹ️ ", postfix: " ℹ️", to: .info)
        emojiLogFormatter.apply(prefix: "⚠️ ", postfix: " ⚠️", to: .warning)
        emojiLogFormatter.apply(prefix: "‼️ ", postfix: " ‼️", to: .error)
        emojiLogFormatter.apply(prefix: "💣 ", postfix: " 💣", to: .severe)
        xcgLogger.formatters = [emojiLogFormatter]

        return true
    }
}
