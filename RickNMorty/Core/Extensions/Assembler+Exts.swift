//
//  Assembler+Exts.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Swinject

extension Assembler {
    func resolveDataStoreFactory() -> DataStoreFactory {
        return resolver.resolve(DataStoreFactory.self)!
    }

    func resolveRickNMortyApiService() -> RickNMortyApiServiceProtocol {
        return resolver.resolve(RickNMortyApiServiceProtocol.self)!
    }

    func resolveSeriesRepository() -> SeriesRepositoryProtocol {
        return resolver.resolve(SeriesRepositoryProtocol.self)!
    }
}
