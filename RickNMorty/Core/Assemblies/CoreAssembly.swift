//
//  CoreAssembly.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Swinject

final class CoreAssembly: RootAssembly {
    // MARK: - Properties

    private let configuration: CoreConfiguration

    // MARK: - Initialization

    init(configuration: CoreConfiguration) {
        self.configuration = configuration
    }

    // MARK: - RootAssembly functions

    override func assemble(container: Container) {
        super.assemble(container: container)

        registerRickNMortyApiService(container)
        registerSeriesRepository(container)
    }

    // MARK: - Functions

    private func registerRickNMortyApiService(_ container: Container) {
        container.register(RickNMortyApiServiceProtocol.self) { _ in
            return RickNMortyApiService()
        }.inObjectScope(.container)
    }

    private func registerSeriesRepository(_ container: Container) {
        container.register(SeriesRepositoryProtocol.self) { resolver in
            let rickNMortyApiService = resolver.resolve(RickNMortyApiServiceProtocol.self)!

            return SeriesRepository(rickNMortyApiService: rickNMortyApiService)
        }.inObjectScope(.container)
    }
}
