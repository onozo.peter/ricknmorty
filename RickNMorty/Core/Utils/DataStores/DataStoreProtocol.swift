//
//  DataStoreProtocol.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Foundation

/**
 * Repositories are storing key-value pairs.
 */
public protocol DataStoreProtocol {
    /**
     * Updates the value for the given key, or stores the value for the given key if the key doesn't exist.
     */
    func setValue<ValueType>(_ value: ValueType, forKey key: String) throws

    /**
     * Returns the value for the given key. Returns nil if the key doesn't exist.
     */
    func value<ValueType>(forKey key: String) throws -> ValueType?

    /**
     * Removes the value for the given key. Does nothing if the key doesn't exist.
     */
    func removeValue(forKey key: String) throws

    /**
     * Removes all the values.
     */
    func removeAllValues() throws
}
