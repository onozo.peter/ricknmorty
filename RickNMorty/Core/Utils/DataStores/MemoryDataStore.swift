//
//  MemoryDataStore.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Foundation

/**
 * Stores key-value pairs in memory.
 */
public class MemoryDataStore {
    // MARK: - Properties

    private var dictionary = [String: Any]()

    // MARK: - Initialization

    public init() {
    }
}

// MARK: - DataStoreProtocol

extension MemoryDataStore: DataStoreProtocol {
    public func setValue<ValueType>(_ value: ValueType, forKey key: String) throws {
        dictionary[key] = value
    }

    public func value<ValueType>(forKey key: String) throws -> ValueType? {
        return dictionary[key] as? ValueType
    }

    public func removeValue(forKey key: String) throws {
        dictionary.removeValue(forKey: key)
    }

    public func removeAllValues() throws {
        dictionary.removeAll()
    }
}
