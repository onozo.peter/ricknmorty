//
//  UserDefaultsDataStore.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

/**
 * Stores key-value pairs in a UserDefaults suite for the given name.
 *
 * From the UserDefaults's documentation: "A default object must be a property list—that is, an instance of (or for collections, a combination of instances of)
 * NSData, NSString, NSNumber, NSDate, NSArray, or NSDictionary. If you want to store any other type of object, you should typically archive it to create an
 * instance of NSData."
 */

import Foundation

public class UserDefaultsDataStore {
    // MARK: - Properties

    private let userDefaults: UserDefaults

    // MARK: - Initialization

    /**
     * Creates a UserDefaults for the given suiteName.
     */
    public init?(suiteName: String) {
        guard let userDefaults = UserDefaults(suiteName: suiteName) else {
            return nil
        }

        self.userDefaults = userDefaults
    }

    /**
     * Using the standard UserDefaults.
     */
    public init() {
        userDefaults = UserDefaults.standard
    }
}

// MARK: - DataStoreProtocol

extension UserDefaultsDataStore: DataStoreProtocol {
    public func setValue<ValueType>(_ value: ValueType, forKey key: String) throws {
        userDefaults.set(value, forKey: key)
    }

    public func value<ValueType>(forKey key: String) throws -> ValueType? {
        return userDefaults.object(forKey: key) as? ValueType
    }

    public func removeValue(forKey key: String) throws {
        userDefaults.removeObject(forKey: key)
    }

    public func removeAllValues() throws {
        let keys = Array(userDefaults.dictionaryRepresentation().keys)
        for key in keys {
            userDefaults.removeObject(forKey: key)
        }
    }
}
