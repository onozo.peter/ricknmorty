//
//  DataStoreDomain.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Foundation

public enum DataStoreDomain: String, CaseIterable, DataStoreDomainProtocol {
    /// Store in UserDefaultsDataStore
    case generalPersistent

    /// Store in MemoryDataStore
    case generalMemory
}
