//
//  Optional+Exts.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Foundation

extension Optional where Wrapped: CustomStringConvertible {
    public var isBlank: Bool {
        return self?.description.isBlank ?? true
    }
}
