//
//  DependencyInjectionAssembler+Exts.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Swinject

extension Assembler {
    public func resetObjectScope(_ objectScope: ObjectScope) {
        (resolver as? Container)?.resetObjectScope(objectScope)
    }
}
