//
//  RootAssemblyProtocol.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Swinject

public protocol RootAssemblyProtocol: Assembly {
}
