//
//  RootAssembly.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Swinject

open class RootAssembly {
    // MARK: - Initialization

    public init() {
    }

    // MARK: - RootAssemblyProtocol functions

    open func assemble(container: Container) {
    }

    open func loaded(resolver: Resolver) {
    }
}

// MARK: - RootAssemblyProtocol

extension RootAssembly: RootAssemblyProtocol {
}
