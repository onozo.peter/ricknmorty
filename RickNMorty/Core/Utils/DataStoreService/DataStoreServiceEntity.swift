//
//  DataStoreServiceEntity.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Foundation

public typealias DataStoreFactory = (DataStoreDomainProtocol) -> DataStoreProtocol?

/**
 * Defines a path (key + domain) for a value in the data store framework.
 */
public struct DataStoreValuePath {
    // MARK: - Properties

    public let key: String
    public let domain: DataStoreDomainProtocol

    // MARK: - Initialization

    public init(key: String, domain: DataStoreDomainProtocol) {
        self.key = key
        self.domain = domain
    }
}
