//
//  DataStoreServiceProtocol.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Foundation

/**
 * A String enum should implement this to enumarate the data store's domains of the application.
 */
public protocol DataStoreDomainProtocol {
    var rawValue: String { get }
}

public protocol DataStoreServiceProtocol {
    /**
     * Removes all the values for the given domain.
     */
    func removeAllValues(forDomain domain: DataStoreDomainProtocol)

    /**
     * Removes all the values for the given domains.
     */
    func removeAllValues(forDomains domains: [DataStoreDomainProtocol])
}
