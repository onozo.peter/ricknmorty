//
//  SeriesRepository.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 21..
//

import RxSwift

final class SeriesRepository: SeriesRepositoryProtocol {
    // MARK: - Properties

    private let rickNMortyApiService: RickNMortyApiServiceProtocol

    // MARK: - Initialization

    init(rickNMortyApiService: RickNMortyApiServiceProtocol) {
        self.rickNMortyApiService = rickNMortyApiService
    }

    // MARK: - Functions

    func character(page: Int) -> Observable<CharacterResponse> {
        return rickNMortyApiService.character(page: page)
    }

    func singleCharacter(for id: Int) -> Observable<SingleCharacterResponse> {
        return rickNMortyApiService.singleCharacter(for: id)
    }

    func singleEpisode(for id: Int) -> Observable<SingleEpisodeResponse> {
        return rickNMortyApiService.singleEpisode(for: id)
    }
}
