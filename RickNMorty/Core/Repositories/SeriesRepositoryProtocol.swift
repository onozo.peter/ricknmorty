//
//  SeriesRepositoryProtocol.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 21..
//

import RxSwift

protocol SeriesRepositoryProtocol {
    func character(page: Int) -> Observable<CharacterResponse>
    func singleCharacter(for id: Int) -> Observable<SingleCharacterResponse>
    func singleEpisode(for id: Int) -> Observable<SingleEpisodeResponse>
}
