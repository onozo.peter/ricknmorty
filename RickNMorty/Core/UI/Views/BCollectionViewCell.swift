//
//  BCollectionViewCell.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 16..
//

import UIKit

class BCollectionViewCell: UICollectionViewCell {
    // MARK: - UIView properties

    override class var requiresConstraintBasedLayout: Bool {
        return true
    }

    // MARK: - Properties

    var shouldUpdateConstrains: Bool = true {
        didSet {
            if shouldUpdateConstrains {
                setNeedsUpdateConstraints()
            }
        }
    }

    // MARK: - UIView initialization

    override init(frame: CGRect) {
        super.init(frame: frame)

        createView()
        initializeView()
    }

    // MARK: - Initialization

    convenience init() {
        self.init(frame: .zero)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UIView functions

    override func updateConstraints() {
        if shouldUpdateConstrains {
            shouldUpdateConstrains = false
            setupConstraints()
        }
        super.updateConstraints()
    }

    // MARK: - Functions

    func createView() {
    }

    func initializeView() {
    }

    func setupConstraints() {
    }
}
