//
//  BTableViewCell.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import UIKit

class BTableViewCell: UITableViewCell {
    // MARK: - UIView properties

    override class var requiresConstraintBasedLayout: Bool {
        return true
    }

    // MARK: - Properties

    var shouldUpdateConstrains: Bool = true {
        didSet {
            if shouldUpdateConstrains {
                setNeedsUpdateConstraints()
            }
        }
    }

    // MARK: - UITableViewCell initialization

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        createView()
        initializeView()

        setNeedsUpdateConstraints()
    }

    // MARK: - Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UIView functions

    override func updateConstraints() {
        if shouldUpdateConstrains {
            shouldUpdateConstrains = false
            setupConstraints()
        }
        super.updateConstraints()
    }

    // MARK: - Functions

    func createView() {
    }

    func initializeView() {
    }

    func setupConstraints() {
    }
}
