//
//  BButton.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 16..
//

import UIKit

class BButton: UIButton {
    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)

        initializeView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Functions

    private func initializeView() {
        backgroundColor = .systemIndigo
        layer.cornerRadius = 24
        clipsToBounds = true

        let icon = UIImage(named: "ButtonCart")!
        setImage(icon, for: .normal)
        setImage(icon, for: .highlighted)
        imageView?.contentMode = .scaleAspectFit
        imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
    }
}
