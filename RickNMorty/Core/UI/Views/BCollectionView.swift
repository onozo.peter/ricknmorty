//
//  BCollectionView.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 16..
//

import UIKit

open class BCollectionView: UICollectionView {
    // MARK: - UICollectionView initialization

    public override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)

        initializeView()
    }

    // MARK: - Initialization

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public convenience init() {
        let layout = UICollectionViewLayout()
        self.init(frame: .zero, collectionViewLayout: layout)
    }

    public convenience init(collectionViewLayout layout: UICollectionViewLayout) {
        self.init(frame: .zero, collectionViewLayout: layout)
    }

    /// Additional initialization of your views
    open func initializeView() {
        isOpaque = false
    }
}
