//
//  UIColor+Exts.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 18..
//

import UIKit

extension UIColor {
    // MARK: - Brand colors

    /// Hex color: #F9F9F9
    static let scLightGray1 = UIColor(hex: "#F9F9F9")

    /// Hex color: #3C3C43
    static let scLightGray2 = UIColor(hex: "#3C3C43")

    /// Hex color: #1C1C1E
    static let scDarkGray = UIColor(hex: "#1C1C1E")
}

extension UIColor {
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        var cleanHexString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if cleanHexString.hasPrefix("#") {
            cleanHexString.remove(at: cleanHexString.startIndex)
        }

        guard cleanHexString.count == 6 else {
            self.init(red: 1, green: 1, blue: 1, alpha: alpha)
            return
        }

        var rgb = UInt64()
        Scanner(string: cleanHexString).scanHexInt64(&rgb)

        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }

    var hexString: String? {
        guard let components = cgColor.components else {
            return nil
        }

        let red = components[0]
        let green = components[1]
        let blue = components[2]

        return String(format: "#%02x%02x%02x", Int(red * 255), Int(green * 255), Int(blue * 255))
    }
}
