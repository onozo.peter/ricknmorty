//
//  BView.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import UIKit

class BView: UIView {
    // MARK: - UIView properties

    override class var requiresConstraintBasedLayout: Bool {
        return true
    }

    // MARK: - Properties

    public var shouldUpdateConstrains = true {
        didSet {
            if shouldUpdateConstrains {
                setNeedsUpdateConstraints()
            }
        }
    }

    private(set) var didSetupConstraints: Bool = false

    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)

        createView()
        initializeView()
    }

    public convenience init() {
        self.init(frame: .zero)
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UIView functions

    override func updateConstraints() {
        if shouldUpdateConstrains {
            shouldUpdateConstrains = false
            setupConstraints()

            didSetupConstraints = true
        }

        super.updateConstraints()
    }

    // MARK: - Functions

    /// Override this function to add subviews.
    open func createView() {
    }

    /// Override this function to add custom initialization to your views. Will be called after the `createView()`.
    open func initializeView() {
        accessibilityIdentifier = String(describing: type(of: self))
    }

    /// Override this function to add auto layout constraints. Set `shouldUpdateConstraints` to `true` to trigger this.
    open func setupConstraints() {
    }
}
