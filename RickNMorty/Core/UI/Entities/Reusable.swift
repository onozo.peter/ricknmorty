//
//  Reusable.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Foundation

protocol Reusable {
    static var reuseID: String { get }
}

extension Reusable {
    static var reuseID: String {
        return String(describing: self)
    }
}
