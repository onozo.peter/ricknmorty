//
//  UITableView+Exts.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import UIKit

extension UITableView {
    public func dequeueReusableCell<T: UITableViewCell>(withIdentifier identifier: String? = nil, for indexPath: IndexPath) -> T {
        let identifier = identifier ?? T.reuseID
        let cell = self.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? T
        switch cell {
        case .some(let unwrapped):
            return unwrapped
        default:
            fatalError("Unable to dequeue" + T.self.description())
        }
    }
}
