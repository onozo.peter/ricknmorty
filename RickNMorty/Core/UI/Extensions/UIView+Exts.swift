//
//  UIView+Exts.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import UIKit

extension UIView {
    /// Returns a collection of constraints to anchor the bounds of the current view to the given view.
    ///
    /// - Parameter view: The view to anchor to.
    /// - Returns: The layout constraints needed for this constraint.
    func constraintsForAnchoringTo(boundsOf view: UIView) -> [NSLayoutConstraint] {
        return [
            topAnchor.constraint(equalTo: view.topAnchor),
            leadingAnchor.constraint(equalTo: view.leadingAnchor),
            view.bottomAnchor.constraint(equalTo: bottomAnchor),
            view.trailingAnchor.constraint(equalTo: trailingAnchor)
        ]
    }

    static func scaledHeight(fromContentWidth contentWidth: CGFloat, designedWidth: CGFloat, designedHeight: CGFloat) -> CGFloat {
        let scale: CGFloat = contentWidth / designedWidth
        let height = designedHeight * scale

        return height
    }
}
