//
//  UICollectionView+Exts.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 16..
//

import UIKit

extension UICollectionView {
    public func dequeueReusableCell<T: UICollectionViewCell>(withReuseIdentifier reuseIdentifier: String? = nil,
                                                             for indexPath: IndexPath) -> T {
        let identifier = reuseIdentifier ?? T.reuseID
        let cell = self.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? T
        switch cell {
        case .some(let unwrapped):
            return unwrapped
        default:
            fatalError("Unable to dequeue" + T.self.description())
        }
    }
}
