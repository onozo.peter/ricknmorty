//
//  UICollectionViewCell+Exts.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 16..
//

import UIKit

extension UICollectionViewCell: Reusable {}
