//
//  RouterProtocol.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import UIKit

protocol Closable: AnyObject {
    func close()
}

protocol RouterProtocol: AnyObject {
    associatedtype V: UIViewController

    var viewController: V? { get }

    func open(_ viewController: UIViewController, transition: Transition)
}
