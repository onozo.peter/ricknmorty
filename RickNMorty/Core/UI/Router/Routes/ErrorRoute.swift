//
//  ErrorRoute.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 16..
//

import RxSwift
import UIKit

protocol AlertRoute {
    func openErrorAlert(error: Error) -> Observable<Int>

    func openAlert(title: String?,
                   message: String?,
                   style: UIAlertController.Style,
                   actions: [UIAlertController.AlertAction]) -> Observable<Int>
}

extension AlertRoute where Self: RouterProtocol {
    func openErrorAlert(error: Error) -> Observable<Int> {
        if let error = error as? RickNMortyApiErrorType {
            if case .networkError = error {
                return openAlert(title: "alert_no_internet_title".localized,
                                 message: "alert_no_internet_message".localized,
                                 style: .alert,
                                 actions: [
                                    UIAlertController.AlertAction(title: "alert_no_internet_retry_btn".localized, style: .default),
                                    UIAlertController.AlertAction(title: "alert_no_internet_cancel_btn".localized, style: .cancel)
                                 ])
            }
        }

        return openAlert(title: "alert_general_title".localized,
                         message: "alert_general_message".localized,
                         style: .alert,
                         actions: [
                            UIAlertController.AlertAction(title: "alert_general_retry_btn".localized, style: .default),
                            UIAlertController.AlertAction(title: "alert_general_cancel_btn".localized, style: .cancel)
                         ])
    }

    func openAlert(title: String?,
                   message: String?,
                   style: UIAlertController.Style,
                   actions: [UIAlertController.AlertAction]) -> Observable<Int> {
        guard let viewController = viewController else {
            return .empty()
        }

        return UIAlertController
            .present(in: viewController, title: title, message: message, style: .alert, actions: actions)
    }
}
