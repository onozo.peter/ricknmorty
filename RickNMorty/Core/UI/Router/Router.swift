//
//  Router.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import UIKit

class Router<U>: RouterProtocol where U: UIViewController {
    typealias V = U

    // MARK: - Properties

    weak var viewController: V?
    var openTransition: Transition?

    // MARK: - RouterProtocol

    func open(_ viewController: UIViewController, transition: Transition) {
        transition.viewController = self.viewController
        transition.open(viewController)
    }

    // MARK: - Closable

    func close() {
        guard let openTransition = openTransition else {
            assertionFailure("You should specify an open transition in order to close a module.")
            return
        }
        guard let viewController = viewController else {
            assertionFailure("Nothing to close.")
            return
        }

        openTransition.close(viewController)
    }
}
