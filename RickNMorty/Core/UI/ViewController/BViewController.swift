//
//  BViewController.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import UIKit
import RxSwift

class BViewController: UIViewController {
    // MARK: - Properties

    let disposeBag = DisposeBag()

    var shouldUpdateViewConstraints = true {
        didSet {
            if shouldUpdateViewConstraints {
                view.setNeedsUpdateConstraints()
            }
        }
    }

    // MARK: - Initialization

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UIViewController functions

    override func loadView() {
        view = createView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        initializeView()
        bindViewModel()
    }

    override func updateViewConstraints() {
        if shouldUpdateViewConstraints {
            shouldUpdateViewConstraints = false
            setupViewConstraints()
        }

        super.updateViewConstraints()
    }

    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    override var shouldAutorotate: Bool {
        return false
    }

    // MARK: - Functions

    /// Add your subviews. Call the super for the root view, add your subviews to it and return the root view.
    func createView() -> BView {
        return BView()
    }

    /// Initialize your views. This will be called in the `viewDidLoad` lifecycle function.
    func initializeView() {
    }

    /// Setup your constraints here. Set `shouldUpdateViewConstraints` to `true` to trigger this.
    func setupViewConstraints() {
    }

    // Add bindigs tot he ViewModel.
    func bindViewModel() {
    }
}
