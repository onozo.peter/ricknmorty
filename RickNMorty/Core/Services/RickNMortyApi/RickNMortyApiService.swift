//
//  RickNMortyApiService.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 20..
//

import RxSwift
import Moya

class RickNMortyApiService {
    // MARK: - GoogleApiServiceProtocol properties

    let provider: MoyaProvider<RickNMortyApi>

    // MARK: - Initialization

    init() {
        provider = MoyaProvider<RickNMortyApi>(
            plugins: [NetworkLoggerPlugin(), CachePolicyPlugin()]
        )
    }

    // MARK: - Functions

    private func request<T: Decodable>(target: RickNMortyApi) -> Observable<T> {
        return provider.rx.request(target)
            .catch { error in
                throw RickNMortyApiErrorType.networkError(error)
            }
            .asObservable()
            .flatMap { response -> Observable<Data> in
                guard (200...299).contains(response.statusCode) else {
                    throw RickNMortyApiErrorType.generic(response.response)
                }

                return .just(response.data)
            }
            .flatMap { data -> Observable<T> in
                do {
                    let content = try JSONDecoder().decode(T.self, from: data)
                    return .just(content)
                } catch {
                    throw RickNMortyApiErrorType.parseError(data)
                }
            }
    }
}

extension RickNMortyApiService: RickNMortyApiServiceProtocol {
    func character(page: Int) -> Observable<CharacterResponse> {
        return request(target: .character(page))
    }

    func singleCharacter(for id: Int) -> Observable<SingleCharacterResponse> {
        return request(target: .singleCharacter(id))
    }

    func singleEpisode(for id: Int) -> Observable<SingleEpisodeResponse> {
        return request(target: .singleEpisode(id))
    }
}
