//
//  RickNMortyServiceEntities.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 20..
//

import Foundation

enum RickNMortyApiErrorType: Error {
    case generic(HTTPURLResponse?)
    case networkError(Error)
    case parseError(Data)
}
