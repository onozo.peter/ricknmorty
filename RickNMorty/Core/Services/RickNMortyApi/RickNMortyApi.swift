//
//  RickNMortyApi.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 20..
//

import Moya

enum RickNMortyApi {
    case character(Int)
    case singleCharacter(Int)
    case singleEpisode(Int)
}

extension RickNMortyApi: TargetType, CachePolicyGettable {
    var baseURL: URL {
        return URL(string: AppSettings.rickNMortyApiSettings.baseUrl)!
    }

    var path: String {
        switch self {
        case .character:
            return "character"
        case .singleCharacter(let id):
            return "character/\(id)"
        case .singleEpisode(let id):
            return "episode/\(id)"
        }
    }

    var method: Method {
        return .get
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .character(let page):
            var params: [String: Any] = [:]
            params["page"] = page
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .singleCharacter:
            return .requestPlain
        case .singleEpisode:
            return .requestPlain
        }
    }

    var headers: [String: String]? {
        return nil
    }

    var cachePolicy: URLRequest.CachePolicy {
        if AppSettings.isDebug {
            return .reloadIgnoringLocalCacheData
        } else {
            return .useProtocolCachePolicy
        }
    }
}
