//
//  SingleCharacterResponse.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 20..
//

import Foundation

struct SingleCharacterResponse: Decodable {
    // MARK: - Properties

    let character: Character

    // MARK: - Initialization

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        character = try container.decode(Character.self)
    }
}
