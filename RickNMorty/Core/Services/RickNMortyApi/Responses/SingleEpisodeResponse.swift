//
//  SingleEpisodeResponse.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 21..
//

import Foundation

struct SingleEpisodeResponse: Decodable {
    // MARK: - Properties

    let episode: Episode

    // MARK: - Initialization

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        episode = try container.decode(Episode.self)
    }
}
