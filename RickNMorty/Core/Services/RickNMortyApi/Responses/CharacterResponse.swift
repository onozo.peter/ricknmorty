//
//  CharacterResponse.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 20..
//

import Foundation

struct CharacterResponse: Decodable {
    // MARK: - Types

    struct Info: Decodable {
        let count: Int
        let pages: Int
        let next: String?
        let prev: String?
    }

    // MARK: - Properties

    let info: Info
    let results: [Character]
}
