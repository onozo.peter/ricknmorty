//
//  RickNMortyApiServiceProtocol.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 20..
//

import RxSwift

protocol RickNMortyApiServiceProtocol {
    func character(page: Int) -> Observable<CharacterResponse>
    func singleCharacter(for id: Int) -> Observable<SingleCharacterResponse>
    func singleEpisode(for id: Int) -> Observable<SingleEpisodeResponse>
}
