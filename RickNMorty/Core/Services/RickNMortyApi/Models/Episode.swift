//
//  Episode.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 21..
//

import Foundation

struct Episode: Decodable {
    // MARK: - Types

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case airDate = "air_date"
        case episode
    }

    // MARK: - Properties

    let id: Int
    let name: String
    let airDate: String
    let episode: String
}
