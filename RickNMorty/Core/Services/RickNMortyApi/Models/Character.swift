//
//  Character.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 21..
//

import Foundation

struct Character: Decodable {
    // MARK: - Types

    struct Origin: Decodable {
        let name: String
    }

    enum Status: String, Codable {
        case alive = "Alive"
        case dead = "Dead"
        case unknown = "unknown"
    }

    // MARK: - Properties

    let id: Int
    let name: String
    let status: Status
    let origin: Origin
    let image: String
}
