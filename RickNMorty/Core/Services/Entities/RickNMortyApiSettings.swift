//
//  RickNMortyApiSettings.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 20..
//

import Foundation

struct RickNMortyApiSettings {
    // MARK: - Properties

    let baseUrl: String
}
