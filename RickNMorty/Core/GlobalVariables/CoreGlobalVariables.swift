//
//  CoreGlobalVariables.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import UIKit
import XCGLogger
import Swinject

public var appDelegate = UIApplication.shared.delegate as! PluggableApplicationDelegate

public let coreAssembler = Assembler()

public func verboseLog(_ closure: @autoclosure () -> Any?,
                       functionName: StaticString = #function,
                       fileName: StaticString = #file,
                       lineNumber: Int = #line,
                       userInfo: [String: Any] = [:]) {
    XCGLogger.default.logln(.verbose, functionName: functionName, fileName: fileName, lineNumber: lineNumber, userInfo: userInfo, closure: closure)
}

public func debugLog(_ closure: @autoclosure () -> Any?,
                     functionName: StaticString = #function,
                     fileName: StaticString = #file,
                     lineNumber: Int = #line,
                     userInfo: [String: Any] = [:]) {
    XCGLogger.default.logln(.debug, functionName: functionName, fileName: fileName, lineNumber: lineNumber, userInfo: userInfo, closure: closure)
}

public func infoLog(_ closure: @autoclosure () -> Any?,
                    functionName: StaticString = #function,
                    fileName: StaticString = #file,
                    lineNumber: Int = #line,
                    userInfo: [String: Any] = [:]) {
    XCGLogger.default.logln(.info, functionName: functionName, fileName: fileName, lineNumber: lineNumber, userInfo: userInfo, closure: closure)
}

public func warningLog(_ closure: @autoclosure () -> Any?,
                       functionName: StaticString = #function,
                       fileName: StaticString = #file,
                       lineNumber: Int = #line,
                       userInfo: [String: Any] = [:]) {
    XCGLogger.default.logln(.warning, functionName: functionName, fileName: fileName, lineNumber: lineNumber, userInfo: userInfo, closure: closure)
}

public func errorLog(_ closure: @autoclosure () -> Any?,
                     functionName: StaticString = #function,
                     fileName: StaticString = #file,
                     lineNumber: Int = #line,
                     userInfo: [String: Any] = [:]) {
    XCGLogger.default.logln(.error, functionName: functionName, fileName: fileName, lineNumber: lineNumber, userInfo: userInfo, closure: closure)
}

public func severeLog(_ closure: @autoclosure () -> Any?,
                      functionName: StaticString = #function,
                      fileName: StaticString = #file,
                      lineNumber: Int = #line,
                      userInfo: [String: Any] = [:]) {
    XCGLogger.default.logln(.severe, functionName: functionName, fileName: fileName, lineNumber: lineNumber, userInfo: userInfo, closure: closure)
}
