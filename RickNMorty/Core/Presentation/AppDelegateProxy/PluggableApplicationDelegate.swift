//
//  PluggableApplicationDelegate.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 15..
//

import Swinject
import CloudKit

open class PluggableApplicationDelegate: UIResponder, UIApplicationDelegate {
    // MARK: - Properties

    open var assemblies: [Assembly] {
        return []
    }

    open var services: [UIApplicationDelegate] {
        return []
    }

    public var orientationLock: UIInterfaceOrientationMask = UIInterfaceOrientationMask.portrait

    private lazy var cachedAssemblies: [Assembly] = {
        return assemblies
    }()

    private lazy var cachedServices: [UIApplicationDelegate] = {
        return services
    }()

    // MARK: - Initialization

    public override init() {
        super.init()
    }

    // MARK: - Functions

    @available(iOS 2.0, *)
    open func applicationDidFinishLaunching(_ application: UIApplication) {
        cachedServices.forEach { $0.applicationDidFinishLaunching?(application) }
    }

    @available(iOS 6.0, *)
    open func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        coreAssembler.apply(assemblies: cachedAssemblies)
        var result = false
        for service in cachedServices {
            if service.application?(application, willFinishLaunchingWithOptions: launchOptions) ?? false {
                result = true
            }
        }
        return result
    }

    @available(iOS 3.0, *)
    open func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        var result = false
        for service in cachedServices {
            if service.application?(application, didFinishLaunchingWithOptions: launchOptions) ?? false {
                result = true
            }
        }
        return result
    }

    @available(iOS 2.0, *)
    open func applicationDidBecomeActive(_ application: UIApplication) {
        for service in cachedServices {
            service.applicationDidBecomeActive?(application)
        }
    }

    @available(iOS 2.0, *)
    open func applicationWillResignActive(_ application: UIApplication) {
        for service in cachedServices {
            service.applicationWillResignActive?(application)
        }
    }

    @available(iOS 2.0, *)
    open func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        for service in cachedServices {
            service.applicationDidReceiveMemoryWarning?(application)
        }
    }

    @available(iOS 2.0, *)
    open func applicationWillTerminate(_ application: UIApplication) {
        for service in cachedServices {
            service.applicationWillTerminate?(application)
        }
    }

    @available(iOS 2.0, *)
    open func applicationSignificantTimeChange(_ application: UIApplication) {
        for service in cachedServices {
            service.applicationSignificantTimeChange?(application)
        }
    }

    @available(iOS 3.0, *)
    open func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        for service in cachedServices {
            service.application?(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
        }
    }

    @available(iOS 3.0, *)
    open func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        for service in cachedServices {
            service.application?(application, didFailToRegisterForRemoteNotificationsWithError: error)
        }
    }

    @available(iOS 7.0, *)
    open func application(_ application: UIApplication,
                          didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                          fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Swift.Void) {
        for service in cachedServices {
            service.application?(application, didReceiveRemoteNotification: userInfo, fetchCompletionHandler: completionHandler)
        }
    }

    @available(iOS 9.0, *)
    open func application(_ application: UIApplication,
                          performActionFor shortcutItem: UIApplicationShortcutItem,
                          completionHandler: @escaping (Bool) -> Swift.Void) {
        for service in cachedServices {
            service.application?(application, performActionFor: shortcutItem, completionHandler: completionHandler)
        }
    }

    @available(iOS 7.0, *)
    open func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Swift.Void) {
        for service in cachedServices {
            service.application?(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
        }
    }

    @available(iOS 8.2, *)
    open func application(_ application: UIApplication,
                          handleWatchKitExtensionRequest userInfo: [AnyHashable: Any]?,
                          reply: @escaping ([AnyHashable: Any]?) -> Swift.Void) {
        for service in cachedServices {
            service.application?(application, handleWatchKitExtensionRequest: userInfo, reply: reply)
        }
    }

    @available(iOS 9.0, *)
    open func applicationShouldRequestHealthAuthorization(_ application: UIApplication) {
        for service in cachedServices {
            service.applicationShouldRequestHealthAuthorization?(application)
        }
    }

    @available(iOS 4.0, *)
    open func applicationDidEnterBackground(_ application: UIApplication) {
        for service in cachedServices {
            service.applicationDidEnterBackground?(application)
        }
    }

    @available(iOS 4.0, *)
    open func applicationWillEnterForeground(_ application: UIApplication) {
        for service in cachedServices {
            service.applicationWillEnterForeground?(application)
        }
    }

    @available(iOS 4.0, *)
    open func applicationProtectedDataWillBecomeUnavailable(_ application: UIApplication) {
        for service in cachedServices {
            service.applicationProtectedDataWillBecomeUnavailable?(application)
        }
    }

    @available(iOS 4.0, *)
    open func applicationProtectedDataDidBecomeAvailable(_ application: UIApplication) {
        for service in cachedServices {
            service.applicationProtectedDataDidBecomeAvailable?(application)
        }
    }

    @available(iOS 6.0, *)
    open func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return orientationLock
    }

    @available(iOS 8.0, *)
    open func application(_ application: UIApplication,
                          shouldAllowExtensionPointIdentifier extensionPointIdentifier: UIApplication.ExtensionPointIdentifier) -> Bool {
        var result = false
        for service in cachedServices {
            if service.application?(application, shouldAllowExtensionPointIdentifier: extensionPointIdentifier) ?? true {
                result = true
            }
        }
        return result
    }

    @available(iOS 6.0, *)
    open func application(_ application: UIApplication,
                          viewControllerWithRestorationIdentifierPath identifierComponents: [String],
                          coder: NSCoder) -> UIViewController? {
        for service in cachedServices {
            if let viewController = service.application?(application, viewControllerWithRestorationIdentifierPath: identifierComponents, coder: coder) {
                return viewController
            }
        }
        return nil
    }

    @available(iOS 13.2, *)
    open func application(_ application: UIApplication, shouldSaveSecureApplicationState coder: NSCoder) -> Bool {
        var result = false
        for service in cachedServices {
            if service.application?(application, shouldSaveSecureApplicationState: coder) ?? false {
                result = true
            }
        }
        return result
    }

    @available(iOS, introduced: 6.0, deprecated: 13.2, message: "Use application:shouldSaveSecureApplicationState: instead")
    open func application(_ application: UIApplication, shouldSaveApplicationState coder: NSCoder) -> Bool {
        var result = false
        for service in cachedServices {
            if service.application?(application, shouldSaveApplicationState: coder) ?? false {
                result = true
            }
        }
        return result
    }

    @available(iOS 13.2, *)
    open func application(_ application: UIApplication, shouldRestoreSecureApplicationState coder: NSCoder) -> Bool {
        var result = false
        for service in cachedServices {
            if service.application?(application, shouldRestoreSecureApplicationState: coder) ?? false {
                result = true
            }
        }
        return result
    }

    @available(iOS, introduced: 6.0, deprecated: 13.2, message: "Use application:shouldRestoreSecureApplicationState: instead")
    open func application(_ application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool {
        var result = false
        for service in cachedServices {
            if service.application?(application, shouldRestoreApplicationState: coder) ?? false {
                result = true
            }
        }
        return result
    }

    @available(iOS 6.0, *)
    open func application(_ application: UIApplication, willEncodeRestorableStateWith coder: NSCoder) {
        for service in cachedServices {
            service.application?(application, willEncodeRestorableStateWith: coder)
        }
    }

    @available(iOS 6.0, *)
    open func application(_ application: UIApplication, didDecodeRestorableStateWith coder: NSCoder) {
        for service in cachedServices {
            service.application?(application, didDecodeRestorableStateWith: coder)
        }
    }

    @available(iOS 8.0, *)
    open func application(_ application: UIApplication, willContinueUserActivityWithType userActivityType: String) -> Bool {
        var result = false
        for service in cachedServices {
            if service.application?(application, willContinueUserActivityWithType: userActivityType) ?? false {
                result = true
            }
        }
        return result
    }

    @available(iOS 8.0, *)
    open func application(_ application: UIApplication,
                          continue userActivity: NSUserActivity,
                          restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Swift.Void) -> Bool {
        var result = false
        for service in cachedServices {
            if service.application?(application, continue: userActivity, restorationHandler: restorationHandler) ?? false {
                result = true
            }
        }
        return result
    }

    @available(iOS 8.0, *)
    open func application(_ application: UIApplication, didFailToContinueUserActivityWithType userActivityType: String, error: Error) {
        for service in cachedServices {
            service.application?(application, didFailToContinueUserActivityWithType: userActivityType, error: error)
        }
    }

    @available(iOS 8.0, *)
    open func application(_ application: UIApplication, didUpdate userActivity: NSUserActivity) {
        for service in cachedServices {
            service.application?(application, didUpdate: userActivity)
        }
    }

    @available(iOS 10.0, *)
    open func application(_ application: UIApplication, userDidAcceptCloudKitShareWith cloudKitShareMetadata: CKShare.Metadata) {
        for service in cachedServices {
            service.application?(application, userDidAcceptCloudKitShareWith: cloudKitShareMetadata)
        }
    }

    @available(iOS 13.0, *)
    open func application(_ application: UIApplication,
                          configurationForConnecting connectingSceneSession: UISceneSession,
                          options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        for service in cachedServices {
            if let sceneConfiguration = service.application?(application, configurationForConnecting: connectingSceneSession, options: options) {
                return sceneConfiguration
            }
        }
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    open func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        for service in cachedServices {
            service.application?(application, didDiscardSceneSessions: sceneSessions)
        }
    }
}
