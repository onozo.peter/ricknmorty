//
//  AppSettings.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 20..
//

import Foundation

var appSettings = AppSettings.init()

struct AppSettings {
    // MARK: - Types

    enum Error: Swift.Error {
        case missingKey, invalidValue
    }

    // MARK: - Properties

    static var configuration: Configuration = {
        do {
            let configuration: String = try AppSettings.value(for: "Configuration")
            let upperConfiguration = configuration.uppercased()
            if upperConfiguration.contains("DEV") {
                if upperConfiguration.contains("DEBUG") {
                    return Configuration.devDebug
                } else {
                    return Configuration.devRelease
                }
            } else if upperConfiguration.contains("PROD") {
                if upperConfiguration.contains("DEBUG") {
                    return Configuration.prodDebug
                } else {
                    return Configuration.prodRelease
                }
            } else {
                assertionFailure("The configuration type is not handled")
            }
        } catch {
            assertionFailure("The Configuration key is missing (\(error)) from Info.plist")
        }

        return .devRelease
    }()

    static var isDebug: Bool = {
        let configuration = AppSettings.configuration
        if configuration == .devDebug || configuration == .prodDebug {
            return true
        }

        return false
    }()

    static var rickNMortyApiSettings: RickNMortyApiSettings {
        return RickNMortyApiSettings(baseUrl: "https://rickandmortyapi.com/api")
    }

    // MARK: - Functions

    static func value<T>(for key: String) throws -> T {
        guard let object = Bundle.main.object(forInfoDictionaryKey: key) else {
            throw Error.missingKey
        }

        switch object {
        case let value as T:
            return value
        default:
            throw Error.invalidValue
        }
    }
}
