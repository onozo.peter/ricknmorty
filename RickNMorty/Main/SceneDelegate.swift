//
//  SceneDelegate.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 20..
//

import UIKit
import RxSwift

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    // MARK: - UIWindowSceneDelegate properties

    var window: UIWindow?

    // MARK: - Initialization

    override init() {
        super.init()

        UIView.appearance().isExclusiveTouch = true
    }

    // MARK: - UIWindowSceneDelegate

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        initWindow(scene)
    }

    private func initWindow(_ scene: UIScene) {
        guard window == nil, let windowScene = scene as? UIWindowScene else {
            return
        }
        let homeModule = HomeModule()

        let navigationController = UINavigationController(rootViewController: homeModule.viewController)

        window = UIWindow(windowScene: windowScene)
        window?.backgroundColor = UIColor.black
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}
