//
//  Configuration.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 20..
//

import Foundation

enum Configuration: String {
    case devDebug
    case devRelease
    case prodDebug
    case prodRelease
}
