//
//  AppDelegate.swift
//  RickNMorty
//
//  Created by Péter Onozó on 2022. 01. 20..
//

import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: PluggableApplicationDelegate {
    // MARK: - PluggableApplicationDelegate properties

    override var assemblies: [Assembly] {
        return [
            UtilsAssembly(),
            CoreAssembly(configuration: CoreConfiguration())
        ]
    }

    override var services: [UIApplicationDelegate] {
        let services: [UIApplicationDelegate] = [
            LoggerAppDelegateService(isDebug: AppSettings.isDebug)
        ]

        return services
    }

    // MARK: - PluggableApplicationDelegate functions

    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        let result = super.application(application, didFinishLaunchingWithOptions: launchOptions)

        return result
    }
}
